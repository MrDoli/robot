#!/usr/bin/env python3
# so that script can be run from Brickman


from time   import time, sleep
from ev3dev.auto import *

s=200
k=10

left_motor = LargeMotor('outA')  
assert left_motor.connected

right_motor = LargeMotor('outD')
assert right_motor.connected

podnosnik = MediumMotor('outC')
assert podnosnik.connected

ts = TouchSensor('in3') 
assert ts.connected

ir = InfraredSensor('in2') 
assert ir.connected
ir.mode='IR-PROX'

colLewy= ColorSensor('in1')
assert colLewy.connected
colLewy.mode = 'COL-COLOR'

colPrawy= ColorSensor('in4')
assert colPrawy.connected
colPrawy.mode = 'COL-COLOR'

btn = Button()

def obrotl():
	left_motor.run_to_rel_pos(position_sp=k*0.6, speed_sp=s*0.6, stop_action="hold")
	right_motor.run_to_rel_pos(position_sp=-k, speed_sp=s, stop_action="hold")
	#sleep(1)
def obrotp():
	left_motor.run_to_rel_pos(position_sp=-k, speed_sp=s, stop_action="hold")
	right_motor.run_to_rel_pos(position_sp=k*0.6, speed_sp=s*0.6, stop_action="hold")
	#sleep(1)
def obrotpaczka1():
	left_motor.run_to_rel_pos(position_sp=-380, speed_sp=s, stop_action="hold")
	right_motor.run_to_rel_pos(position_sp=380, speed_sp=s, stop_action="hold")
	#sleep(1)
def obrotpaczka2():
	left_motor.run_to_rel_pos(position_sp=180*0.8, speed_sp=s*0.8, stop_action="hold")
	right_motor.run_to_rel_pos(position_sp=-180, speed_sp=s, stop_action="hold")
	#sleep(1)
def prosto():
	left_motor.run_forever(speed_sp=-s)
	right_motor.run_forever(speed_sp=-s)

def do_tylu():
	left_motor.run_to_rel_pos(position_sp=200, speed_sp=s, stop_action="hold")
	right_motor.run_to_rel_pos(position_sp=200, speed_sp=s, stop_action="hold")
def do_tylu1():
	left_motor.run_to_rel_pos(position_sp=50, speed_sp=s, stop_action="hold")
	right_motor.run_to_rel_pos(position_sp=50, speed_sp=s, stop_action="hold")
def podnoszenie():
	podnosnik.run_to_rel_pos(position_sp=-1900, speed_sp=2*s, stop_action="hold")

def opuszczanie():
	podnosnik.run_to_rel_pos(position_sp=1900, speed_sp=2*s, stop_action="hold")

def run():
	paczka=0
	pierwsza_lewo=0
	bialy=6
	czarny=1
	czerwony=5
	zielony_lewy=3
	zielony_prawy=2
	transporter=1
	kolor_lewy=3
	IR=4
	while not ts.value():
		odczytLewy=colLewy.value()
		odczytPrawy=colPrawy.value()
		odczytIR=ir.value()
		#podnoszenie paczki
		if (ir.value()<IR and paczka==0 and transporter==1):
			right_motor.run_forever(speed_sp=0)
			left_motor.run_forever(speed_sp=0)
			pierwsza_lewo=1
			paczka=1
			kolor_lewy=czerwony
			podnoszenie()
			sleep(10)
			obrotpaczka1()
			sleep(4)
		#opuszczanie paczki
		elif (odczytLewy==czerwony and odczytPrawy==czerwony and paczka==1):
			right_motor.run_forever(speed_sp=0)
			left_motor.run_forever(speed_sp=0)
			sleep(2)
			do_tylu()
			sleep(2)
			opuszczanie()
			sleep(10)
			do_tylu1()
			sleep(1)
			obrotpaczka1()
			sleep(4)
			paczka=0
			pierwsza_lewo=1
			transporter=0
		#skręt w lewo gdy wyjeżdża z paczkš
		elif (odczytLewy!=bialy and odczytPrawy!=bialy and pierwsza_lewo==1):
			obrotpaczka2()
			sleep(1)
			pierwsza_lewo=0
			print ("skret w lewo gdy wyjezdza z paczka")
		#prosto
		elif (odczytLewy==bialy and odczytPrawy==bialy):
			prosto()
		#prosto na skrzyżowniu
		elif (odczytLewy==czarny and odczytPrawy==czarny and pierwsza_lewo==0):
			prosto()
		#prosto gdy z paczkš zielony
		elif (odczytPrawy==zielony_lewy and odczytPrawy==bialy and (paczka==1 or transporter==0) and pierwsza_lewo==0):
			prosto()
			print("prosto gdy z paczka zielony")
		#prosto gdy bez paczki czerwony
		elif (odczytLewy==czerwony and odczytPrawy==bialy and paczka==0 and pierwsza_lewo==0):
			prosto()
			print("prosto gdy bez paczki czerwony")
		#obrót lewo normalny
		elif (odczytLewy==czarny and odczytPrawy==bialy):
			obrotl()
		#obrót lewo gdy na wyjedzie z zielonego na zielonym
		elif (odczytLewy==zielony_lewy and odczytPrawy==bialy and pierwsza_lewo==1 and paczka==1):
			obrotl()
		#obrót lewo gdy na wyjedzie z czerwonego na czerwonym
		elif (odczytLewy==czerwony and odczytPrawy==bialy and pierwsza_lewo==1):
			obrotl()
		#obrót prawo normalny
		elif (odczytLewy==bialy and odczytPrawy==czarny):
			obrotp()
		#obrót prawo gdy na wyjedzie z zielonego na zielonym
		elif (odczytLewy==bialy and odczytPrawy==zielony_prawy and pierwsza_lewo==1 and paczka==0):
			obrotp()
		#obrót prawo gdy na wyjedzie z czerwonego na czerwonym
		elif (odczytLewy==bialy and odczytPrawy== czerwony and pierwsza_lewo==1):
			obrotp()
		#obrót w prawo gdy lewy najechał na zielony, prawy na czarny a nie powinien skręcić
		elif (odczytLewy==zielony_lewy and odczytPrawy==czarny and (paczka==1 or transporter==0)):
			obrotp()
			print("obrot w prawo gdy lewy najechal na zielony, prawy na czarny a nie powinien skrecie")
		#obrót w prawo gdy lewy najechał na czerwony, prawy na czarny a nie powinien skręcić
		elif (odczytLewy==czerwony and odczytPrawy==czarny and paczka==0):
			obrotp()
		#w lewo na zielonym gdy bez paczki
		elif (odczytLewy==zielony_lewy and odczytPrawy!=zielony_prawy and paczka==0 and transporter==1):
			obrotl()
			print("w lewo na zielonym gdy bez paczki")
		#w lewo na czerwonym gdy z paczkš
		elif (odczytLewy==czerwony and odczytPrawy!=czerwony and paczka==1):
			obrotl()
		elif (odczytLewy==bialy and odczytPrawy!=bialy):
			obrotp()
			print("inne w prawo")
		elif (odczytLewy!=bialy and odczytPrawy==bialy):
			obrotl()
			print("inne w lewo")
		else:
			prosto()
			print("else")
		  
	Sound.beep() 
	right_motor.run_forever(speed_sp=0)
	left_motor.run_forever(speed_sp=0)
	podnosnik.run_forever(speed_sp=0)

run()
right_motor.run_forever(speed_sp=0)
left_motor.run_forever(speed_sp=0)	
podnosnik.run_forever(speed_sp=0)
